Before do |_scn|
  $exceptions ||= []

  # Set short-hand list
  @api = MethodApi.new
end

After do |scn|
  $exceptions << scn.exception.message if scn.failed?
  # Add debug to demo env
  binding.pry if ENV['demo']
  # Add debug for debug env with failed scenario
  binding.pry if scn.failed? && ENV['debug']
end

at_exit do
  $exceptions ||= []

  # Generate report
  File.write('../.exceptions.log', $exceptions) unless $exceptions.empty?
end
