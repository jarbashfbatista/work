Dado('que eu verifico api') do
  $host = EL['url_default']

  response = @api.get($host)
  @response_code = response.code
  raise 'API esta fora' if @response_code != 200
end

Quando('eu faco um GET no endpoint {string}') do |endpoint|
  @url = $host + endpoint

  response = @api.get(@url)
  @response      = response
  @response_code = response.code
  @response_msg  = response.message
end

Quando ('eu mostro a respota {string}') do |camp|
  p @response[camp]
end

Dado('eu espero que o endpoint retorne {int}') do |code|
  raise @response_msg if @response_code != code
end

Entao('eu espero o corpo da respota') do
  raise 'respota vazia' if @response == {}
end

Entao ('eu espero o corpo da respota tenha um erro') do
  erro = "{\n  \"erro\": true\n}"
  raise 'CEP existente' if @response.to_s != erro
end
