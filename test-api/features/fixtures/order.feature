#language: pt
Funcionalidade: API Test

Contexto: API estar CEP ok
Dado que eu verifico api

Cenario: Sucesso na consulta, printando o código do IBGE do endereço no stdout.
Dado eu faco um GET no endpoint "/ws/01001000/json/"
E eu espero que o endpoint retorne 200
Quando eu mostro a respota "ibge"
Entao eu espero o corpo da respota

Cenario: Sucesso na consulta, passando um CEP inválido.
Dado eu faco um GET no endpoint "/ws/01001009/json/"
E eu espero que o endpoint retorne 200
Entao eu espero o corpo da respota tenha um erro



#bonus
Cenario: Erro na consulta, passando um CEP inválido.
Dado eu faco um GET no endpoint "/ws/0100/json/"
Entao eu espero que o endpoint retorne 400

