## Getting Started ##

### Requirements

* Ruby (version v2.5 or higher)
* Bundler (Gem)

### Installing gems ###
To install gems type:
```shell
bundle install
```
### Run tests in DEV###
Type this in the tests folder:
```shell
bundle exec cucumber
```
### Run with tags###
Type this in the tests folder:
```shell
bundle exec cucumber --tags @run
```